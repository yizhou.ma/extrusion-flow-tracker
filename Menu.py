import numpy as np
from tkinter import *
import functions
from tkinter import filedialog
import pandas as pd

window = Tk()
window.title("Flow Tracker")
window.geometry("800x400")

#### Functions ####


# Function: Call calibraton function; Set factor text
def calibrate(file, known):
    if not file:
        textStatusBar.set("Error: Select a file first")
        statusbar.configure(background='grey')
    elif known == '':
        textStatusBar.set("Error: Select a known width first")
        statusbar.configure(background='grey')
    elif float(known) <= 0:
        textStatusBar.set("Error: Select a known width first")
        statusbar.configure(background='grey')
    else:
        textStatusBar.set(("Opening", file, "to calibrate pixel width with known size: ", known))
        factorOF.set(functions.calibrateKnown(file, float(known)))
        if float(factorOF.get()) <= 0:
            textStatusBar.set("Calibration factor has not been set")
            statusbar.configure(background='grey')
        elif float(factorOF.get()) <= 0:
            textStatusBar.set("Set a calibration factor")
            statusbar.configure(background=window["background"])
        else:
            textStatusBar.set("Calibration factor has been set")
            statusbar.configure(background=window["background"])

# Function: Call calibraton function; Set factor text
def selectWidthBox(file, scale):
    global widthBoxPos
    if not file:
        textStatusBar.set(("Error: Select a file first"))
        statusbar.configure(background='grey')
    elif scale == '':
        textStatusBar.set("Error: Set a scale first")
        statusbar.configure(background='grey')
    else:
        textStatusBar.set(("Opening", file, "to select box for measuring stream width"))
        widthBoxPos = functions.getWidthBox(file, float(scale))
        if widthBoxPos is False:
            textStatusBar.set("No bounding box selected")
            statusbar.configure(background='grey')
        else:
            textStatusBar.set("Bounding box selected")
            statusbar.configure(background=window["background"])

# Function: Call VelocityTrack function; Save velocity data in array; Update factor text
def trackPoints(file, factor, scale):
    global pointsTrack, frameTime, widthBoxPos
    if not file:
        textStatusBar.set(("Error: Select a file first"))
        statusbar.configure(background='grey')
    elif scale == '':
        textStatusBar.set("Error: Set a scale first")
        statusbar.configure(background='grey')
    elif startTime.get() == '':
        textStatusBar.set("Error: Select a correct starting time")
        statusbar.configure(background='grey')
    elif factorOF.get() == '':
        textStatusBar.set("Error: Select a correct calibration factor")
        statusbar.configure(background='grey')
    else:
        if float(factorOF.get()) <= 0:
            textStatusBar.set("Error: Select a correct calibration factor")
            statusbar.configure(background='grey')
        else:
            textStatusBar.set("Opening tracking window")
            statusbar.configure(background=window["background"])
            pointsTrackAll, frameTime, streamWidthAll = functions.velocityTrack(file, factor, float(scale), float(startTime.get()), float(cutoffDistance.get()), widthBoxPos)
            pointsTrack = []
            if len(pointsTrackAll[0]) > 0:
                textStatusBar.set("Successfully tracked extrusion")
                statusbar.configure(background=window["background"])
                arePointsTracked.set("Points are tracked")
                lb_tracked.config(fg="green")
                bt_save.config(state="normal")
                for i in pointsTrackAll:
                    for ii in i:
                        pointsTrack.append(ii)
                getVelocity(pointsTrackAll, float(factorOF.get()), float(scaleOF.get()), frameTime, streamWidthAll)
            else:
                textStatusBar.set("Unable to track points")
                statusbar.configure(background='grey')
                arePointsTracked.set("No extrusion tracked")
                lb_tracked.config(fg="red")
                bt_save.config(state="disabled")


# Function: Calculate relevant variables from the velocity data; Set resulting variable text
def getVelocity(points, factor, scale, frameTime, streamWidthAll):
    global distanceOF, distanceBatchOF, timeOF, normVelocityOF, normTimeOF, streamWidthOF
    streamWidthOF = []
    distanceOF, distanceBatchOF, timeOF, normVelocityOF, normTimeOF, streamWidthOF = functions.calculateVelocity(points, factor, scale, frameTime, streamWidthAll)

    # Updating labels
    textVelocityAvgOF.set("Average velocity: " + str(round(np.average(normVelocityOF), 3)) + str(unit.get()) + "/s")
    textTimeOF.set("Time taken: " + str(round(max(timeOF), 3)) + "s")


# Function: Calls plotResult functoin to plot the results
def plotPoints(velocity, time, numS, streamWidth):
    functions.plotResult(velocity, time, numS, streamWidth)


# Function: Saves results to a csv file
def saveResults(data, titles):
    saveFile = filedialog.asksaveasfilename(initialdir = folder.get(),
                                      title = "Save results",
                                      defaultextension = ".csv")
    if saveFile == None:
        textStatusBar.set(("Select a file to save to"))
        statusbar.configure(background='grey')
    else:
        textStatusBar.set(("Saving data to: ", saveFile))
        statusbar.configure(background='grey')
        dfData = pd.DataFrame([pd.Series(value) for value in data]).transpose()
        dfData.columns = titles
        dfData.to_csv(saveFile, index=False)


# Function: File browser for selecting the video to be analyzed
def browseFiles():
    filedr = filedialog.askopenfilename(#initialdir = folderVid,
                                          title = "Select a video",
                                          filetypes = (("Video files",
                                                        "*.avi*"),
                                                       ("all files",
                                                        "*.*")))
    newpath = filedr.split('/')[-1]
    lb_file.configure(text="File Opened: " + newpath)
    fileName.set(filedr)
    newPath.set(newpath)


#Function: Check if value is a float
def isfloat(e, var):
    if var == knownVar:
        resetValues(knownVar)
    elif var == factorOF:
        resetValues(factorOF)
    n = var.get()+str(e.char)
    try:
        n = float(n)
    except ValueError:
        if e.char != "\x08" and e.char != "":
            return "break"


#Function: Update labels when unit is changed. Calibration factor is also reset when unit changes
def labelUpdate(*args):
    knownVarText.set(("Known distance: [" + str(unit.get()) + "]"))
    cutoffText.set("Cutoff distance [" + str(unit.get()) + "]:")
    resetValues()


#Function: When calibration is altered, data is discarded to prevent miscalculations
def resetValues(var):
    global widthBoxPos, pointsTrack, frameTime, distanceOF, distanceBatchOF, timeOF, normVelocityOF, normTimeOF
    if var != factorOF:
        factorOF.set(0)  # default factor
    widthBoxPos = False
    pointsTrack = []
    frameTime = 1.00000
    arePointsTracked.set("No tracked points selected")

    # Calculation results
    distanceOF = [0]
    distanceBatchOF = []
    timeOF = []
    normVelocityOF = []
    normTimeOF = []

    # Text variables
    textVelocityAvgOF.set("Average velocity: " + str(0))
    textTimeOF.set("Time taken: " + str(0) + "s")
    lb_tracked.config(fg="red")
    bt_save.config(state="disabled")




###############




#### Variables ####

# Set starting time
startTime = StringVar(window)
startTime.set(0)

### change save to a file explorer
folder = StringVar(window)
folder.set("Results/")

# file explorer for loading videos
#folderVid = "Microscopic videos/"
fileName = StringVar(window)
newPath = StringVar(window)

# known calibration and factor sizing
knownVar = StringVar(window)
units = ['mm', 'cm', 'm']
unit = StringVar(window)
unit.set(units[0])
factorOF = StringVar(window)
factorOF.set(0)  # default factor
textFactor = StringVar(window)
textFactor.set(("Calibration factor"))
knownVarText = StringVar(window)
knownVarText.set(("Known width [" + str(unit.get()) + "]:"))

# Variable for scale of window; standard of 1
scaleOF = StringVar(window)
scaleOF.set('1')

# Variables for points tracked; time per frame
pointsTrack = []
frameTime = 1.00000
arePointsTracked = StringVar(window)
arePointsTracked.set("No tracked points selected")

# Variable for stream width
widthBoxPos = False
streamWidthOF = []

# Variable for cutoff distance
cutoffText = StringVar(window)
cutoffText.set("Cutoff distance [" + str(unit.get()) + "]:")
cutoffDistance = StringVar(window)
cutoffDistance.set(0.5)

# Variables for calculation results
distanceOF = [0]
distanceBatchOF = []
timeOF = []
normVelocityOF = []
normTimeOF = []

# Variable for smoothing
numS = StringVar(window)
numS.set(21)

# Text variables
textVelocityAvgOF = StringVar(window)
textVelocityAvgOF.set("Average velocity: " + str(0) + str(unit.get()) + "/s")
textTimeOF = StringVar(window)
textTimeOF.set("Time taken: " + str(0) + "s")
textStatusBar = StringVar(window)
textStatusBar.set("...")

# Variables for save result
whatToSave = [newPath.get(), factorOF.get(), scaleOF.get(), frameTime, timeOF, pointsTrack, distanceOF, distanceBatchOF, normVelocityOF, normTimeOF, streamWidthOF]
# Include a variable table in README

header = ["file_name",
          "calibration_factor",
          "video_scale",
          "seconds_per_frame_s",
          "raw_time_s",
          "points_tracked",
          "distance_mm",
          "distance_per_batch_mm",
          "velocity_mm_s-1",
          "time_s",
          "stream_width_mm"
          ]

##############


#### Tkinter layout ####


# Row 1: Select file and image scale
lb_file = Label(window, text="Select a video:")
dr_file = Button(window, text="Open to browse", command=browseFiles)
lb_scale = Label(window, text="Video scale:")
en_scale = Entry(window, textvariable=scaleOF)
en_scale.bind('<KeyPress>', lambda e: isfloat(e, scaleOF))

lb_file.grid(row=1, column=0, pady=5, padx=20, sticky=W)
dr_file.grid(row=1, column=1, pady=5, padx=20, sticky=W)
lb_scale.grid(row=1, column=2, pady=5, padx=20, sticky=W)
en_scale.grid(row=1, column=3, pady=5, padx=20, sticky=W)


# Row 2: Select known width and Width unit
lb_known = Label(window, textvariable=knownVarText)
en_known = Entry(window, textvariable=knownVar)
en_known.bind('<KeyPress>', lambda e: isfloat(e, knownVar))
dr_unit = OptionMenu(window, unit, *units)
unit.trace('w', labelUpdate)

lb_known.grid(row=2, column=0, pady=5, padx=20, sticky=W)
en_known.grid(row=2, column=1, pady=5, padx=20, sticky=W)
dr_unit.grid(row=2, column=2, pady=5, padx=20, sticky=W)


# Row 3: Calibrate pixel factor
lb_cal = Label(window, text="Calibrate pixel factor:")
bt_cal = Button(window, text="Calibrate", command=lambda: calibrate(fileName.get(), knownVar.get()))
lb_factor = Label(window, textvariable=textFactor)
en_factor = Entry(window, textvariable=factorOF)
en_factor.bind('<KeyPress>', lambda e: isfloat(e, factorOF))

lb_cal.grid(row=3, column=0, pady=5, padx=20, sticky=W)
bt_cal.grid(row=3, column=1, pady=5, padx=20, sticky=W)
lb_factor.grid(row=3, column=2, columnspan=2, pady=15, padx=20, sticky=W)
en_factor.grid(row=3, column=3, columnspan=2, pady=15, padx=20, sticky=W)


# Row 4: Calibrate stream width & Start time
lb_width = Label(window, text="Measure extrudate width")
bt_width = Button(window, text="Select ROI", command=lambda: selectWidthBox(fileName.get(), scaleOF.get()))

lb_startTime = Label(window, text="Start of video [s]")
en_startTime = Entry(window, textvariable=startTime)
en_startTime.bind('<KeyPress>', lambda e: isfloat(e, startTime))

lb_width.grid(row=4, column=0, pady=5, padx=20, sticky=W)
bt_width.grid(row=4, column=1, pady=5, padx=20, sticky=W)
lb_startTime.grid(row=4, column=2, pady=5, padx=20, sticky=W)
en_startTime.grid(row=4, column=3, pady=5, padx=20, sticky=W)


# Row 5: Track stream velocity
lb_track = Label(window, text="Track extrusion rate")
bt_track = Button(window, text="Track", command=lambda: trackPoints(fileName.get(), float(factorOF.get()), scaleOF.get()))
lb_trackdistance = Label(window, textvariable=cutoffText)
en_trackdistance = Entry(window, textvariable=cutoffDistance)
en_trackdistance.bind('<KeyPress>', lambda e: isfloat(e, cutoffDistance))

lb_track.grid(row=5, column=0, pady=5, padx=20, sticky=W)
bt_track.grid(row=5, column=1, pady=5, padx=20, sticky=W)
lb_trackdistance.grid(row=5, column=2, pady=5, padx=20, sticky=W)
en_trackdistance.grid(row=5, column=3, pady=5, padx=20, sticky=W)


# Row 6&7: Show Data
lb_tracked = Label(window, textvariable=arePointsTracked, fg="red")
lb_time = Label(window, textvariable=textTimeOF)
lb_vel_avg = Label(window, textvariable=textVelocityAvgOF)

lb_tracked.grid(row=6, column=0, rowspan=2, pady=15, padx=(20, 0), sticky=W)
lb_time.grid(row=6, column=2, pady=(15, 0), padx=20, sticky=W)
lb_vel_avg.grid(row=7, column=2, pady=(0, 15), padx=20, sticky=W)


# Row 8: Plot data
lb_plot = Label(window, text="Plot results")
bt_plot = Button(window, text="Plot", command=lambda: plotPoints(normVelocityOF, normTimeOF, int(numS.get()), streamWidthOF))
lb_nums = Label(window, text="Smoothing factor")
en_nums = Entry(window, textvariable=numS)
en_nums.bind('<KeyPress>', lambda e: isfloat(e, numS))

lb_plot.grid(row=8, column=0, pady=5, padx=20, sticky=W)
bt_plot.grid(row=8, column=1, pady=5, padx=20, sticky=W)
lb_nums.grid(row=8, column=2, pady=5, padx=20, sticky=W)
en_nums.grid(row=8, column=3, pady=5, padx=20, sticky=W)


# Row 9: Save data to csv
bt_save = Button(window, text="Export data to csv", command=lambda: saveResults([newPath.get(), float(factorOF.get()), float(scaleOF.get()), frameTime, timeOF, pointsTrack, distanceOF, distanceBatchOF, normVelocityOF, normTimeOF, streamWidthOF], header), state="disabled")

bt_save.grid(row=9, column=1, pady=5, padx=20, sticky=W)


# Status bar
statusbar = Label(window, textvariable=textStatusBar, relief=SUNKEN, anchor=W, pady=2, padx=10)
statusbar.place(rely=1, relx=0.5, anchor=S, relwidth=1)

##############

window.mainloop()
