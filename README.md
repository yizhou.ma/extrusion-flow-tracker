# Extrusion Flow Tracker

This is a software that was developed by the Food Process Engineering Group at Wageningen University to measure 3D food printing extrusion flow and die-swell. 

## User Guide

The current version of the scripts can only be run with the required python modules installed (see requirements.txt for details). We recommend running the scripts in PyCharm or other IDE. Menu.py opens the UI for the program, which includes file a browsing functionality to select videos to measure. One example video (cheese-50C.avi) is available to download into your drive. A quick video tutorial of the software can be found [here](https://youtu.be/caQpmhgCi-0). 

## Citation

This work has been published in a paper. Please cite our work as 
Ma, Y., Potappel, J., Chauhan, A., Schutyser, M. A., Boom, R. M., & Zhang, L. (2022). Improving 3D food printing performance using computer vision and feedforward nozzle motion control. Journal of Food Engineering, 111277.

## Contact

If you have questions about the software or would like to contribute to this open-source software development, please contact us at yizhou.ma@wur.nl
