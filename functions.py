import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
import pims
from scipy.signal import savgol_filter
import traceback

## Add entry field for cutoff line
## add entry field for smoothing parameters - These are just used for quick visualisation


#### PIXEL SIZE CALCULATION ####
def getLinePoints(event, x, y, flags, param):
    #This method is called in the calibrateKnown function to draw the horizontal points of the known width
    global knownPts
    if event == cv2.EVENT_LBUTTONDOWN:
        if len(knownPts) < 2:
            knownPts.append((round(x), round(y)))


def calibrateKnown(file, known):
    #This method is called to get the mm per pixel
    global knownPts
    knownPts = []
    mmPerPixel = 0
    allFrames = pims.Video(file)
    fps = allFrames.frame_rate
    frameIdx = 1
    play = False

    while True:
        cv2.namedWindow("FrameCal")
        cv2.setMouseCallback("FrameCal", getLinePoints)
        if play:
            frameIdx += 1
        if frameIdx >= len(allFrames):
            frameIdx = len(allFrames) - 1
            play = False
        if frameIdx < 0:
            frameIdx = 0

        frame = allFrames[frameIdx]
        if len(knownPts) == 1:
            cv2.circle(frame, knownPts[0], 2, (255, 255, 255), -1)
        if len(knownPts) == 2:
            cv2.line(frame, knownPts[0], (knownPts[1][0], knownPts[0][1]), (255, 255, 255), 2)
            widthInPixels = abs(knownPts[0][0] - knownPts[1][0])
            mmPerPixel = round((known / widthInPixels), 5)

        cv2.imshow("FrameCal", frame)
        k = cv2.waitKeyEx(1)
        time.sleep(0.05)

        # User inputs
        # q or Esc = quit
        # SPACEBAR or p = pause/play
        # r = rewind to start
        # + = next frame
        # - = previous frame
        # LEFT_ARROW = go back 1 seconds
        # RIGHT_ARROW = go forward 1 seconds

        if k == ord('q') or k == ord('\x1b'):
            cv2.destroyWindow("FrameCal")
            if mmPerPixel > 0:
                return mmPerPixel
            else:
                return 0
        if k == ord(' ') or k == ord('p'):
            play ^= True
        if k == ord('r'):
            knownPts = []
            mmPerPixel = 0
        if k == ord('-') or k == ord('-'):
            frameIdx -= 1
        if k == ord('=') or k == ord('+'):
            frameIdx += 1
        if k == 2555904:
            frameIdx += fps
        if k == 2424832:
            frameIdx -= fps


def getBox(event, x, y, flags, param):
    global ptsBox, tempPtsBox
    if len(ptsBox) < 2:
        x = round(x)
        y = round(y)
        if event == cv2.EVENT_LBUTTONDOWN:
            ptsBox.append((x, y))
            if len(ptsBox) == 2:
                # Make sure corners of the box are ordered correctly
                A = (min((ptsBox[0][0], ptsBox[1][0])), min((ptsBox[0][1], ptsBox[1][1])))
                B = (max((ptsBox[0][0], ptsBox[1][0])), max((ptsBox[0][1], ptsBox[1][1])))
                ptsBox = [A, B]
        elif event == cv2.EVENT_MOUSEMOVE:
            tempPtsBox = (x, y)


def getWidthBox(file, scale):
    #This method is called to select the box for width measurement
    global ptsBox, tempPtsBox
    ptsBox = []
    tempPtsBox = ()
    allFrames = pims.Video(file)
    fps = allFrames.frame_rate
    frameIdx = round(len(allFrames)/2) # Start at halfway through the video
    play = False

    while True:
        cv2.namedWindow("FrameBox")
        cv2.setMouseCallback("FrameBox", getBox)
        if play:
            frameIdx += 1
        if frameIdx >= len(allFrames):
            frameIdx = len(allFrames) - 1
            play = False
        if frameIdx < 0:
            frameIdx = 0

        frame = allFrames[frameIdx]
        frame = cv2.resize(frame, (0, 0), None, 1 * scale, 1 * scale)
        if len(ptsBox) == 1:
            cv2.rectangle(frame, ptsBox[0], tempPtsBox, (255, 255, 255), 1)
        if len(ptsBox) == 2:
            cv2.rectangle(frame, ptsBox[0], ptsBox[1], (255, 255, 255), 1)

        cv2.imshow("FrameBox", frame)

        k = cv2.waitKeyEx(1)
        time.sleep(0.05)

        # User inputs
        # q or Esc = quit
        # SPACEBAR or p = pause/play
        # r = rewind to start
        # + = next frame
        # - = previous frame
        # LEFT_ARROW = go back 1 seconds
        # RIGHT_ARROW = go forward 1 seconds

        if k == ord('q') or k == ord('\x1b'):
            cv2.destroyWindow("FrameBox")
            if len(ptsBox) == 2:
                return ptsBox
            else:
                return False
        if k == ord(' ') or k == ord('p'):
            play ^= True
        if k == ord('r'):
            ptsBox = []
        if k == ord('-') or k == ord('-'):
            frameIdx -= 1
        if k == ord('=') or k == ord('+'):
            frameIdx += 1
        if k == 2555904:
            frameIdx += fps
        if k == 2424832:
            frameIdx -= fps

################


# Select point for tracking by clicking
def selectPoint (event, x, y, flags, params):
    #This method is called by velocityTrack to select a point
    global point, pointSelected, oldPoints, pointsTrackBatch, pointsTrackAll, streamWidthAll, streamWidth, box
    if event == cv2.EVENT_LBUTTONDOWN:
        if len(pointsTrackBatch) > 0:
            pointsTrackAll.append(pointsTrackBatch)
            if box is not False:
                streamWidthAll.append(streamWidth)
        pointsTrackBatch = []
        x = round(x)
        y = round(y)
        point = (x, y)
        pointSelected = True
        oldPoints = np.array([[x, y]], dtype=np.float32)

# Calculate average width of the stream from a Canny filter image
def getWidth(img):
    # a and b are the most left and right white pixels in each row
    a = np.argmax(img > 0, axis=1)
    b = img.shape[1]-(np.argmax(np.flip(img, axis=1) > 0, axis=1))-1
    w = []
    width = 0
    for i in range(len(a)):
        # remove all instances where no pixels were found
        if a[i] > 0 and b[i] < (img.shape[1]-1):
            # minimal distance between a and b: 10 pixels
            if (b[i] - a[i]) > 10:
                w.append(b[i] - a[i])
    if len(w) > 0:
        width = np.mean(w)
    return width


# Important function #
# this method uses opticalFlow to track the pixel movement of a point
def velocityTrack(file, factor, scale, printTime, cutoffDistance, boxPts):
    global point, pointSelected, oldPoints, pointsTrackBatch, pointsTrackAll, streamWidthAll, streamWidth, box
    box = boxPts
    pointSelected = False
    point = ()
    oldPoints = np.array([[]])
    streamWidthAll = []
    streamWidthBatch = []
    pointsTrackBatch = []
    pointsTrackAll = []
    allFrames = pims.Video(file)
    fps = allFrames.frame_rate
    firstFrame = int(printTime * fps)
    timePerFrame = round(allFrames.duration/len(allFrames), 7)
    frameIdx = firstFrame
    clicked = False
    play = False

    # Parameters for the Lucas-Kanade method
    lk_params = dict(winSize=(25, 25),
                     maxLevel=2,
                     criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 15, 0.05))

    # If a width box is selected: Creates a new window showing the width of the stream
    if boxPts is not False:
        boxParams = 200
        cv2.namedWindow("Param")
        cv2.resizeWindow("Param", 320, 200)
        cv2.createTrackbar("Threshold1", "Param", 50, 255, (lambda a: None))
        cv2.createTrackbar("Threshold2", "Param", 120, 255, (lambda a: None))

    # Loop of openCV
    while True:
        cv2.namedWindow("FrameTrack")
        cv2.setMouseCallback("FrameTrack", selectPoint)
        if play:
            frameIdx += 1
        if frameIdx >= len(allFrames):
            frameIdx = len(allFrames) - 1
            play = False
        if frameIdx < 0:
            frameIdx = 0

        # Load frame. If there is a corrupt frame, an error prints and the frame is skipped
        try:
            frame = allFrames[frameIdx]
        except Exception:
            traceback.print_exc()
            # Current batch is saved
            if len(pointsTrackBatch) > 0:
                pointsTrackAll.append(pointsTrackBatch)
                pointsTrackBatch = []
                oldPoints = np.array([point], dtype=np.float32)

                if boxPts is not False:
                    streamWidthAll.append(streamWidthBatch)
                    streamWidthBatch = []
                continue

        # Optical flow algorithm requires grayed versions of the current and previous frame.
        oldFrame = allFrames[(frameIdx - 1)]
        frame = cv2.resize(frame, (0, 0), None, 1 * scale, 1 * scale)
        oldFrame = cv2.resize(oldFrame, (0, 0), None, 1 * scale, 1 * scale)
        oldGray = cv2.cvtColor(oldFrame, cv2.COLOR_BGR2GRAY)
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)


        ### Width measurements ###
        if boxPts is not False:
            # Crop and blur a previously selected part of the frame for width measurements
            widthBox = frame[boxPts[0][1]:boxPts[1][1], boxPts[0][0]:boxPts[1][0]]
            boxBlur = cv2.GaussianBlur(widthBox, (7, 7), 0)
            boxGray = cv2.cvtColor(boxBlur, cv2.COLOR_BGR2GRAY)
            # Get thresholds for Canny filter from trackbars
            boxThreshold1 = cv2.getTrackbarPos("Threshold1", "Param")
            boxThreshold2 = cv2.getTrackbarPos("Threshold2", "Param")
            # Apply Canny filter for edge detection
            boxCanny = cv2.Canny(boxGray, boxThreshold1, boxThreshold2)

        ###

        # Time counter
        cv2.putText(frame, str(round(frameIdx * timePerFrame, 2)) + "s / " + str(round(allFrames.duration, 2)) + "s", (10, 20), cv2.FONT_HERSHEY_COMPLEX, .7, (0, 255, 0), 2)

        # Tracking loop
        if pointSelected:
            # Visualisations
            cv2.circle(frame, point, 5, (0, 255, 255), 2)
            cutoff = int(point[1] + cutoffDistance / factor * scale)
            cv2.line(frame, (0, cutoff), (frame.shape[1], cutoff), (255, 0, 0), 2)

            if play:
                #Optical Flow tracking algorithm
                newPoints, status, error = cv2.calcOpticalFlowPyrLK(oldGray, grayFrame, oldPoints, None,
                                                                    **lk_params)
                x, y = newPoints.ravel()
                x = round(x)
                y = round(y)

                # If the point has not moved down:
                if y <= (oldPoints[0][1]):
                    print("no velocity found on frame", frameIdx)
                    # Save current batch if it exists and start a new batch at the original point
                    if len(pointsTrackBatch) > 0:
                        pointsTrackAll.append(pointsTrackBatch)
                        pointsTrackBatch = []
                        oldPoints = np.array([point], dtype=np.float32)

                        if boxPts is not False:
                            streamWidthAll.append(streamWidthBatch)
                            streamWidthBatch = []
                # If the point succesfully moves down
                else:
                    oldPoints = newPoints
                    cv2.circle(frame, (x, y), 5, (0, 255, 0), -1,)
                    # The stream width is calculated
                    if boxPts is not False:
                        streamWidth = getWidth(boxCanny)
                        streamWidthBatch.append(streamWidth)
                    pointsTrackBatch.append([x, y, frameIdx])

                    # Save current batch if the point has moved below the cutoff
                    if len(pointsTrackBatch) > 0:
                        if pointsTrackBatch[-1][1] >= cutoff:
                            pointsTrackAll.append(pointsTrackBatch)
                            pointsTrackBatch = []
                            oldPoints = np.array([point], dtype=np.float32)

                            if boxPts is not False:
                                streamWidthAll.append(streamWidthBatch)
                                streamWidthBatch = []
            if 'newPoints' in locals():
                cv2.circle(frame, (x, y), 5, (0, 255, 0), -1,)
            else:
                cv2.circle(frame, (point), 5, (0, 255, 0), -1,)

        # If a width box has been defined, the stream outline is displayed in the Param window
        if boxPts is not False:
            # Add black border to cropped box if it is small to ensure minimal width of 320 for the window
            boxWidth = boxCanny.shape[1]
            if 320 - round(boxWidth) >= 2:
                pad = round((320 - round(boxWidth)) / 2)
                boxCannyBorder = cv2.copyMakeBorder(boxCanny, 0, 0, pad, pad, cv2.BORDER_CONSTANT, value=(0, 0, 0))
                cv2.imshow("Param", boxCannyBorder)     #Preserve original image: border cannot affect width calculations
            else:
                cv2.imshow("Param", boxCanny)

        cv2.imshow("FrameTrack", frame)
        k = cv2.waitKeyEx(1)

        # User inputs
        # q or Esc = quit
        # SPACEBAR or p = pause/play
        # r = rewind to start
        # + = next frame
        # - = previous frame
        # LEFT_ARROW = go back 1 seconds
        # RIGHT_ARROW = go forward 1 seconds

        if k == ord('q') or k == ord('\x1b'):
            cv2.destroyWindow("FrameTrack")
            if boxPts is not False:
                cv2.destroyWindow("Param")
                streamWidthAll.append(streamWidthBatch)
            allFrames = []
            pointsTrackAll.append(pointsTrackBatch)
            return pointsTrackAll, timePerFrame, streamWidthAll
        if k == ord(' ') or k == ord('p'):
            play ^= True
        if k == ord('r'):
            frameIdx = firstFrame
        if k == ord('-') or k == ord('-'):
            frameIdx -= 1
        if k == ord('=') or k == ord('+'):
            frameIdx += 1
        if k == 2555904:
            frameIdx += fps
        if k == 2424832:
            frameIdx -= fps


# Velocity calculation from tracked points using pythagoras
def pixelVelocity(pt1, pt2, f, t):
    a = pt1[0]-pt2[0]
    b = pt1[1]-pt2[1]
    d = np.sqrt((a**2)+(b**2))*f
    v = b*f/t

    return d, v


# Calculate velocity from tracked points. Tracked points are tracked in batches. Average of each batch is calculated
def calculateVelocity(pts, factor, scale, timePerFrame, streamWidth):
    # pts is a list of lists: List of batches; Each batch has multiple points
    factor = factor/scale
    velocity = []
    velocityTime = []
    distance = []
    distanceBatch = []
    streamWidthPx = []
    for item in range(0, len(pts)):  # First run through all batches
        lastPoint = ()
        tempVelocity = []
        tempTime = []
        tempWidth = []
        for i in pts[item]:  # Run through every individual point in the batch
            if len(lastPoint) > 0:  # Calculations can't be done on the first point
                tempVelocity.append(pixelVelocity(i, lastPoint, factor, timePerFrame)[1])  # Velocity is appended to a temporary list for later rounding
                tempTime.append(round(i[2]*timePerFrame, 7))    # Time is calculated from the frame count
                if len(distance) > 0:
                    d = pixelVelocity(i, lastPoint, factor, timePerFrame)[0] + distance[-1]
                else:
                    d = pixelVelocity(i, lastPoint, factor, timePerFrame)[0]
                distance.append(d)
            lastPoint = i
        if len(streamWidth) > 0:
            if type(streamWidth[item]) == list:
                for x in streamWidth[item]:
                    tempWidth.append((x * factor))  # Width in mm is calculated from the pixel width
            else:
                tempWidth.append((streamWidth[item]*factor))
        if len(tempVelocity) > 0:  # After all velocities in the batch are calculated, the batch is averaged and saved
            velocity.append(np.average(tempVelocity))
            velocityTime.append(np.average(tempTime))
            distanceBatch.append(distance[-1])
            if len(tempWidth) > 0:
                streamWidthPx.append(np.average(tempWidth))
    time = []
    # A timestamp for all points is calculated
    for item in pts:
        timeTemp = []
        for i in item:
            timeTemp.append(round(i[2]*timePerFrame, 7))
        time.append(np.average(timeTemp))
    return distance, distanceBatch, time, velocity, velocityTime, streamWidthPx


# Plot results with 2 subplots: Smoothed data & Normal data
def plotResult(velocity, time, numS, width):
    x_new, y_new = smoothGraph(time, velocity, numS)
    nozzlePos = []
    for i in range(0, len(x_new)):
        if len(nozzlePos) == 0:
            nozzlePos.append((x_new[i]*y_new[i]))
        else:
            pos = nozzlePos[-1] + ((x_new[i]-x_new[i-1]) * y_new[i])
            nozzlePos.append(pos)

    if len(width) > 0:
        plt.subplot(131)
        plt.plot(x_new, y_new)
        plt.ylabel('Velocity [mm/s]')
        plt.xlabel('Time [s]')
        plt.suptitle('Velocity over time')
        plt.subplot(132)
        plt.plot(nozzlePos, y_new)
        plt.ylabel('Velocity [mm/s]')
        plt.xlabel('Nozzle position [mm]')
        plt.suptitle('Velocity over nozzle position')
        plt.tight_layout(pad=1.0)
        plt.subplot(133)
        plt.plot(time, width)
        plt.ylabel('Stream width [mm]')
        plt.xlabel('Time [s]')
        plt.suptitle('Stream width over time')
        plt.show()
    else:
        plt.subplot(121)
        plt.plot(x_new, y_new)
        plt.ylabel('Velocity [mm/s]')
        plt.xlabel('Time [s]')
        plt.suptitle('Velocity over time')
        plt.subplot(122)
        plt.plot(nozzlePos, y_new)
        plt.ylabel('Velocity [mm/s]')
        plt.xlabel('Nozzle position [mm]')
        plt.suptitle('Velocity over nozzle position')
        plt.tight_layout(pad=1.0)
        plt.show()


#Savitzkt-Golay smoothing
def smoothGraph(x, y, numS):
    if int(len(y)/30) > numS:
        numS = int(len(x)/30)
        if (numS % 2) == 0:
            numS = numS - 1
    yhat = savgol_filter(y, numS, 1)
    return x, yhat

